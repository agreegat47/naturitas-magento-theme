module.exports = {
    sassDir:          'scss/',
    sassMainFileName: 'styles',
    cssDir:           'css/',
    cssMainFileDir:   'css/',
    cssMainFileName:  'styles',
    jsDir:            'js/',
    imgDir:           'images/',
    imgSourceDir:     'sourceimages/',

    // sftp server
    sftpServer:       'example.com',
    sftpPort:         '2121',
    sftpLogin:        'login',
    sftpPas:          'password',
    sftpDestination:  '/pathTo/css'
};